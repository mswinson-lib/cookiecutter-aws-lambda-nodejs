# cookiecutter-aws-lambda-nodejs

generates a project to build a nodejs lambda 

## Prequisites

    python
    cookiecutter

## Usage

    cookiecutter bb:mswinson-lib/cookiecutter-aws-lambda-nodejs.git


## Contributing

1. Fork it ( https://bitbucket.org/mswinson-lib/cookiecutter-aws-lambda-nodejs.git )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new pull request
